# Jut-dl
A simple anime downloader from Jut.su

This script allows you to download anime from the site
To use it, download the required dependencies

`pip install requests beautifulsoup4`

Now clone this repository

`git clone https://github.com/KirMozor/Jut-dl/`

Or download as .zip

Then run the downjutsu .py file

`python3 jutdl.py`

Next follow the instructions
